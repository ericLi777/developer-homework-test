import {
    GetProductsForIngredient,
    GetRecipes,
} from "./supporting-files/data-access";
// import { NutrientFact, Product } from "./supporting-files/models";
import {
    GetCostPerBaseUnit,
    GetNutrientFactInBaseUnits,
} from "./supporting-files/helpers";
import { RunTest, ExpectedRecipeSummary } from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
let recipeSummary: any = {}; // the final result to pass into the test function
/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */

// get productName
function getRecipesName() {
    const recipeName = recipeData.map((item) => item.recipeName);
    return recipeName;
}
const recipeName = getRecipesName()[0];
console.log(recipeName);

// get the ingredient by recipe name
function getIngredientofProduct(productName: string) {
    const product = recipeData.filter(
        (item) => item.recipeName === productName
    );
    const ingredientItemofProduct = product.map((item) =>
        item.lineItems.map((item) => item.ingredient)
    );
    return ingredientItemofProduct;
}
getIngredientofProduct("Creme Brulee");
// console.log(getIngredientofProduct("Creme Brulee"));

// get the ingredtient info from supplier by productName
function getIngredientFromSupplier(productName: string) {
    const getIngredientInfo = getIngredientofProduct(productName)[0].map(
        (item) => GetProductsForIngredient(item)
    );

    return getIngredientInfo;
}
getIngredientFromSupplier("Creme Brulee");

function getCheapestCost(productName: string) {
    const data = getIngredientFromSupplier(productName);
    let cheapestCost = null;
    let cheapestProduct = null;
    let costArray = [];
    for (const arr of data) {
        for (const obj of arr) {
            const supplierProducts = obj.supplierProducts.map((item) =>
                GetCostPerBaseUnit(item)
            );
            //compare the price to get the cheapest one
            supplierProducts.sort((a, b) => a - b);
            if (cheapestCost === null || supplierProducts[0]) {
                cheapestCost = supplierProducts[0];
                cheapestProduct = {
                    productName: obj.productName,
                    brandName: obj.brandName,
                    nutrientFacts: obj.nutrientFacts,
                    LowestCost: cheapestCost,
                };
                costArray.push(cheapestProduct);
            }
        }
    }
    return costArray;
}
getCheapestCost("Creme Brulee");
// console.log(getCheapestCost("Creme Brulee"));

//filter words from supplier ingredients to get the lowest cost
function getLowestCostByIngradients(ingradients: string) {
    const costData = getCheapestCost("Creme Brulee");
    const ingradientsLowest = costData.filter((product) =>
        product.productName.toLowerCase().includes(ingradients.toLowerCase())
    );
    ingradientsLowest.sort((a, b) => a.LowestCost - b.LowestCost);
    return ingradientsLowest[0];
}
// get only the receipt neeeded ingradient with supply info
function getEachIngredientCheapestCost(productName: string) {
    const ingredientData = getIngredientofProduct(productName)[0].map((item) =>
        getLowestCostByIngradients(item.ingredientName)
    );
    return ingredientData;
}
getEachIngredientCheapestCost("Creme Brulee");

// get the cost of each ingredient
const recipeLineItems = recipeData[0].lineItems;
const supplierIngredients = getEachIngredientCheapestCost("Creme Brulee");
const result = recipeLineItems.map((p) => {
    const matchingSupplierProduct = supplierIngredients.find((sp) =>
        sp.productName
            .toLowerCase()
            .includes(p.ingredient.ingredientName.toLowerCase())
    );
    if (matchingSupplierProduct) {
        return {
            ingredientName: p.ingredient.ingredientName,
            cost:
                matchingSupplierProduct.LowestCost * p.unitOfMeasure.uomAmount,
        };
    } else {
        return {
            ingredientName: p.ingredient.ingredientName,
            cost: 0,
        };
    }
});

//total cost of the recipe
const totalCost = result.reduce((acc, curr) => acc + curr.cost, 0);
const totalCostresult = { cheapestCost: totalCost };
console.log("totalCost:", totalCostresult);

// get the nutrient facts of getCheapestCost('Creme Brulee')
const cheapestIngrandientList = getCheapestCost("Creme Brulee");
// console.log(
//     GetNutrientFactInBaseUnits(cheapestIngrandientList[0].nutrientFacts[0])
// );
const nutrientSum = {};
for (let product of cheapestIngrandientList) {
    for (let nutrient of product.nutrientFacts) {
        GetNutrientFactInBaseUnits(nutrient);
        const nutrientName = nutrient.nutrientName;
        let quantityAmount = nutrient.quantityAmount.uomAmount;
        // const quantityPer = nutrient.quantityPer.uomAmount;
        // quantityAmount = quantityAmount / quantityPer;
        if (nutrientName in nutrientSum) {
            nutrientSum[nutrientName] += quantityAmount;
        } else {
            nutrientSum[nutrientName] = quantityAmount;
        }
    }
}
const nutrientsAtCheapestCost = {};
for (let nutrientName of Object.keys(nutrientSum).sort()) {
    const quantityAmount = nutrientSum[nutrientName];
    nutrientsAtCheapestCost[nutrientName] = {
        nutrientName: nutrientName,
        quantityAmount: {
            uomAmount: quantityAmount,
            uomName: "grams",
            uomType: "mass",
        },
        quantityPer: { uomAmount: 100, uomName: "grams", uomType: "mass" },
    };
}
console.log(nutrientsAtCheapestCost);

//final output result
recipeSummary = {
    [recipeName]: {
        cheapestCost: totalCostresult,
        nutrientsAtCheapestCost,
    },
};

console.log(recipeSummary);

/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
